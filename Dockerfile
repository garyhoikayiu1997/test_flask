# FROM debian:jessie-slim

# RUN apt-get update && apt-get install -y \
#     git \
#     python3 \
#     python3-pip

# RUN pip3 install Flask gunicorn

# ENV LC_ALL C.UTF-8
# ENV LANG C.UTF-8

# WORKDIR /app

# COPY . /app

FROM python:3.9

WORKDIR /app
COPY . /app
RUN pip3 install -r ./requirements.txt
CMD gunicorn --bind=0.0.0.0:8000 wsgi:app